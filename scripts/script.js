if (typeof window !== "undefined") {
    window.addEventListener("load", function () {
        let currentChoice;
        let userNumber = +prompt("Enter your number");

        while (!userNumber || isNaN(userNumber) || userNumber < 0){
            currentChoice = userNumber
            userNumber = +prompt("re-enter your number", `You have previously chose ${currentChoice}`)
        }

        console.log(factorial(userNumber))

        function factorial(num) {
            let result = 1;
            if (num === 0){
                return result;
            }
            for (let i = result; i < num+1; i++) {
                result *= i;
            }
            return result;
        }
    });
}
